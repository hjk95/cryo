package com.srk.training;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
/* WordCount.java
 * Purpose : Counting number of words and Counting unique words
 * Read file using FileReader
 * Used While loops for counting number of word and counting unique words
 * Used list collection for unique word count
 * */
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;

 
class WordCount {
	//enter into main method
public static void main(String[] args) throws Exception {
	
System.out.println("Read File");
FileReader fileRead = new FileReader("C:/HashMap.txt");//file reading syntax
BufferedReader read = new BufferedReader(fileRead);
String line = "", str = "";//initialize string
int count = 0;//initialize count
int linesInc = 0;
System.out.println("Enter into 1st while loop");
while ((line = read.readLine()) != null) {
str += line + " ";
linesInc++;
}
System.out.println("Exit from  1st while loop");
StringTokenizer token = new StringTokenizer(str);//make object of StringTokenize
System.out.println("Enter into 2nd while loop");
while (token.hasMoreTokens()) {
String s = token.nextToken();
count++;//increment count
}
System.out.println("Exit from 2nd while loop");
System.out.println("File has " + count + " words are in the file");
System.out.println("exit from 1st logic");
FileWriter fstream = new FileWriter("D:/out.txt"); 
BufferedWriter out = new BufferedWriter(fstream);
String input;
input="total words : "+ count;
out.write(input);
out.flush();
//out.close();


//Counting unqiue words
System.out.println("Entering into try block");

try {
    List<String> list = new ArrayList<String>();//Create List of String
    int totalWords = 0;//initialize totalWords
    //int uniqueWords = 0;//initialize UniqueWords
    File fr1 = new File("C:/HashMap.txt");
    //FileOutputStream fos = new FileOutputStream(fr1);
    //PrintStream pos = new PrintStream(fos);
    //System.setOut(pos);
    Scanner sc = new Scanner(fr1);//Scanning File
    System.out.println("Entering into 3rd While loop");
    while (sc.hasNext()) {
        String words = sc.next();
        String[] space = words.split(" ");
        for (int i = 0; i < space.length; i++) {
            list.add(space[i]);
        }
        totalWords++;
        
    }
    System.out.println("Exit from 3rd while loop");
   
    Set<String> uniqueSet = new HashSet<String>(list);
    System.out.println("Printing words frequency");
    //FileWriter wr = new FileWriter("Answer.txt");
    //BufferedWriter br2 = new BufferedWriter(wr);
    
    
    
    for (String word : uniqueSet) {
    	int i= Collections.frequency(list,word);
        String SN = (word + ": " + Collections.frequency(list,word)); 
        FileWriter fstream2 = new FileWriter("D:/out.txt"); 
        BufferedWriter out2 = new BufferedWriter(fstream2);
        out.write(SN);
             
    }
    out.flush();
    out.close();
} catch (Exception e) {}
System.out.println("Exit from program");
 
}
}	