package com.srk;
import java.util.Properties;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.engines.BlowfishEngine;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Base64;

//import First.Program.AppTest;
	 
	public class SecurityUtil extends Properties {
		private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
	  String originalString = "Vijaya Bandhekar"; 
      String keyString = "secretkey";
	  try {
	   System.out.println("Enter into encryption");
	   String encryptedString = encrypt(originalString, keyString);
	   System.out.println("This is the original string");
	   System.out.println("Original String: " + originalString);
	   System.out.println("This is the encrypted string");
	   System.out.println("Encrypted String: " + encryptedString);
	  
	  } catch (Exception e) {
	   e.printStackTrace();
	  }
	 }
	 
	 public static String encrypt(String value, String keyString)
	  throws Exception {
	  BlowfishEngine engine = new BlowfishEngine();
	  PaddedBufferedBlockCipher cipher =
	   new PaddedBufferedBlockCipher(engine);
       KeyParameter key = new KeyParameter(keyString.getBytes());
	   cipher.init(true, key);
	  byte in[] = value.getBytes();
	  byte out[] = new byte[cipher.getOutputSize(in.length)];
	  int len1 = cipher.processBytes(in, 0, in.length, out, 0);
	  try {
	   cipher.doFinal(out, len1);
	  } catch (CryptoException e) {
	   e.printStackTrace();
	   throw new Exception(e.getMessage());
	  }
	  String s = new String(Base64.decode(keyString));
	  return s;
	 }
	}