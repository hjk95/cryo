package com.srk.training;
/*
 * QueueImplementation.java
 * @param(int)this method adds element at the end of the queue.
 * @param() this method pop elements from the queue.
 */
import java.util.Scanner;
public class QueueImplementation {
     
    private int limit;//Limit of stack
    int queueArr[];//array of queue
    int front = 0;
    int rear = -1;
    int currentSize = 0;//current size of queue
     //constructor
    public QueueImplementation(int queueSize){
        this.limit = queueSize;
        queueArr = new int[this.limit];
    }
 
   //parameterized constructor
    public void enqueue(int input) {
    	System.out.println("Enter into enqueue");
        if (isQueueFull()) {
            System.out.println("Overflow ! Unable to add element: "+input);
        } else {
            rear++;//increases rear
            if(rear == limit-1){
                rear = 0;
            }
            queueArr[rear] = input;
            currentSize++;//increase current size
            System.out.println("Element " + input + " is pushed to Queue !");
            
        }
        System.out.println("Exit form enqueue");
    }
    
    
 
    /**
     * this method removes an element from the top of the queue
     */
    public void dequeue() {
    	System.out.println("Enter into dequeue");
        if (isQueueEmpty()) {
            System.out.println("Underflow ! Unable to remove element from Queue");
        } else {
            front++;//increment front when pop
            if(front == limit-1){
                System.out.println("Pop operation done ! removed: "+queueArr[front-1]);
                front = 0;
            } else {
                System.out.println("Pop operation done ! removed: "+queueArr[front-1]);
            }
            currentSize--;//decrement current size
        }
        System.out.println("Exit form dequeue");
    }
 
    /*
     * This method checks whether the queue is full or not
     * @return boolean
     */
    public boolean isQueueFull(){
        boolean status = false;
        if (currentSize == limit){
            status = true;
        }
        return status;
    }
     
    /**
     * This method checks whether the queue is empty or not
     * @return
     */
    public boolean isQueueEmpty(){
        boolean status = false;
        if (currentSize == 0){
            status = true;
        }
        return status;
    }
     //main method
    public static void main(String a[]){
        //Scanner sc = new Scanner(System.in);
        QueueImplementation queue = new QueueImplementation(4);
        queue.enqueue(4);
        queue.dequeue();
        queue.enqueue(56);
        queue.enqueue(2);
        queue.enqueue(67);
        queue.dequeue();
        queue.dequeue();
        queue.enqueue(24);
        queue.dequeue();
    }
}

