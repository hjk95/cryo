var inputString="";
var operators = ['+', '-', 'x', '/', '%'];
function displayInput(num){
  if(num=='+' || num=='-' || num=='x' || num=='/' || num=='%'){
    if(inputString[(inputString.length-1)]=='+' || inputString[(inputString.length-1)]=='-' || inputString[(inputString.length-1)]=='x' || inputString[(inputString.length-1)]=='/' || inputString[(inputString.length-1)]=='%'){
      inputString = inputString.slice(0, -1) + num;
    }
    else{
      inputString=searchDelimiter(inputString)+num;
    }
  }
  else if (num=='=') {
    inputString=searchDelimiter(inputString).toString();
  }
  else{
    inputString=inputString+num;
  }
  displayIt(inputString);
}
function searchDelimiter(inputString1){
  var i,f,n,final;
  for(i=0;i<operators.length;i++){
    n=inputString1.indexOf(operators[i]);
    if(n>0){
      f=i;
    }
    else {
      final=inputString1;
    }
  }
  if(f<(operators.length+1)){
    final=operatorFetch(f,inputString1)
  }
  return final;
}
function operatorFetch(count,inputString1){
  var a,b;
  a=inputString1.split(operators[count])[0];
  b=inputString1.split(operators[count])[1];
  switch(operators[count]){
    case '+':final=addNumbers(a,b);
    break;
    case '-':final=subtractNumbers(a,b);
    break;
    case 'x':final=multiplyNumbers(a,b);
    break;
    case '/':final=divisionNumber(a,b);
    break;
    case '%':final=percentageNumbers(a,b);
    break;
  }
  return final;
}
function addNumbers(a,b){
  var na=parseFloat(a);
  var nb=parseFloat(b);
  return parseFloat((na + nb).toFixed(2));
}
function subtractNumbers(a,b){
  var na,nb;
  na=parseFloat(a);
  nb=parseFloat(b);
  return parseFloat((na - nb).toFixed(2));
}
function percentageNumbers(a,b){
  var na,nb;
  na=parseFloat(a);
  nb=parseFloat(b);
  return parseFloat(((na * nb)/100).toFixed(2));
}
function multiplyNumbers(a,b){
  var na,nb;
  na=parseFloat(a);
  nb=parseFloat(b);
  return parseFloat((na * nb).toFixed(2));
}
function divisionNumber(a,b){
  var na,nb;
  na=parseFloat(a);
  nb=parseFloat(b);
  return parseFloat((na / nb).toFixed(2));
}
function displayIt(n){
  var r = document.getElementById('result');
  r.innerHTML=n;
}
function clearEverything(){
  inputString="";
  document.getElementById('result').innerHTML=inputString;
}
