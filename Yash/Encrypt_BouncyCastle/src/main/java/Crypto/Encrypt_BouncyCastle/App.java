package Crypto.Encrypt_BouncyCastle;

import java.util.Scanner;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.engines.BlowfishEngine;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Base64;


public class App 
{
    public static void main( String[] args )
    {
    	
    	  System.out.println("Enter string to encrypt: ");
    	  Scanner s = new Scanner(System.in);
    	  
    	  String plainText = s.nextLine();
    	  System.out.println("Log 1: plain text entered ");
    	  System.out.println("Enter key value: ");
    	  
    	  String keyString = s.nextLine();
    	  System.out.println("Log 2: key value entered ");
    	  
    	  try {
    		  System.out.println("Log 3: Entry in main try block ");
    	   String encryptedString = encrypt(plainText, keyString);
    	   System.out.println("Log 5: encryption done successfully ");
    	   
    	   System.out.println("Original String :" +" "+ plainText+ " " + "legnth of plain text:" +plainText.length());
    	   System.out.println("Encrypted String : " + encryptedString +" "+ "legnth of encrypted text :" + encryptedString.length());
    	   System.out.println("Log 6: entry in decrypt method");
    	   System.out.println("Log 7: Decryption done successfully");
    	   
    	   String decryptedString = decrypt(encryptedString, keyString);
    	   
    	   
    	   System.out.println("Decrypted String: " + decryptedString +" "+ "legnth of decrypted text :" + decryptedString.length());
    	  }
    	  catch (Exception e) {
    		  System.out.println("Log 8: Entry in main catch block ");
    	   e.printStackTrace();
    	  }
    	  System.out.println("Log 10: program successfully done");
    	 }
    	 
    	 public static String decrypt(String name, String keyString)
    	  throws Exception {
    		 
    	  BlowfishEngine engine = new BlowfishEngine();
    	  PaddedBufferedBlockCipher cipher =
    	   new PaddedBufferedBlockCipher(engine);
    	  StringBuffer result = new StringBuffer();
    	  KeyParameter key = new KeyParameter(keyString.getBytes());
    	  cipher.init(false, key);
    	  byte out[] = Base64.decode(name);
    	  byte out2[] = new byte[cipher.getOutputSize(out.length)];
    	  int len2 = cipher.processBytes(out, 0, out.length, out2, 0);
    	  cipher.doFinal(out2, len2);
    	  String s2 = new String(out2);
    	  for (int i = 0; i < s2.length(); i++) {
    	   char c = s2.charAt(i);
    	   if (c != 0) {
    	    result.append(c);
    	   }
    	  }
    	 
    	  return result.toString();
    	 }
    	 
    	 public static String encrypt(String value, String keyString)
    	  throws Exception {
    		 System.out.println("Log 4: Entry in Encrypt method");
    	  BlowfishEngine engine = new BlowfishEngine();
    	  PaddedBufferedBlockCipher cipher =
    	   new PaddedBufferedBlockCipher(engine);
    	  KeyParameter key = new KeyParameter(keyString.getBytes());
    	  cipher.init(true, key);
    	  byte in[] = value.getBytes();
    	  byte out[] = new byte[cipher.getOutputSize(in.length)];
    	  int len1 = cipher.processBytes(in, 0, in.length, out, 0);
    	  try {
    	   cipher.doFinal(out, len1);
    	  } catch (CryptoException e) {
    	   e.printStackTrace();
    	   throw new Exception(e.getMessage());
    	  }
    	  
    	  String s = new String(Base64.encode(out));
    	  
    	  
    	  return s;
    	 }
    	}

