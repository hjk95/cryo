package com.srk.training;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
	import java.util.Collections;
	import java.util.HashSet;
	import java.util.List;
	import java.util.Scanner;
	import java.util.Set;

	//import org.apache.log4j.Logger;



	/**
	 * Hello world!
	 *
	 */
	public class UniqueAndWordCountExample
	{
	    public static void main( String[] args ) throws Exception 
	    {
	    	// final Logger logger = Logger.getLogger(Uniqcount.class);
	         //logger.info("Counting Words");   
	     	
	         FileReader fr = new FileReader ("C:/HashMap.txt");  //read the file      
	         BufferedReader br = new BufferedReader (fr); //read the text from a character-based input stream    
	         String line = br.readLine ();//read data line by line
	         int count = 0;
	         while (line != null) {//check the condition line is not empty
	            String []parts = line.split(" ");//split word based on blank space
	            for( String w : parts)//count word
	            {
	              count++;        
	            }
	            line = br.readLine();
	         }         
	         System.out.println(count);//print count value
	         FileWriter fw=new FileWriter("C:/Answer.txt");
	         BufferedWriter bw =new BufferedWriter(fw);
	        String input;
	        input="total words:"+count;
	         bw.write(input);
	         bw.flush();
	         bw.close();
	    	try {
	            List<String> list = new ArrayList<String>();
	            int totalWords = 0;
	            int uniqueWords = 0;
	            File fr1 = new File("C:/HashMap.txt");
	            Scanner sc = new Scanner(fr1);
	            
	            while (sc.hasNext()) {
	                String words = sc.next();
	                String[] space = words.split(" ");
	                for (int i = 0; i < space.length; i++) {
	                    list.add(space[i]);
	                }
	                totalWords++;
	            }
	            //logger.info("Words with their count..");
	            Set<String> uniqueSet = new HashSet<String>(list);
	            for (String word : uniqueSet) {
	                System.out.println(word + ": " + Collections.frequency(list,word));
	            }
	        } catch (Exception e) {

	            //logger.info("File not found");

	        }
	    	FileWriter fw2=new FileWriter("C:/Answer2.txt");
	         BufferedWriter bw2 =new BufferedWriter(fw2);
	         
	    }
	}

