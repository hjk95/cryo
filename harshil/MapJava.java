import java.util.*;

public class MapJava {
	static HashMap<Integer,String> map=new HashMap<Integer,String>();  
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int choice;
		System.out.println("Menu:");
		System.out.println("1. Create Map");
		System.out.println("2. Iterate Map");
		System.out.println("3. Sort by Keys");
		System.out.println("4. Sort by Values");
		System.out.println("5. Exit");
		System.out.print("Choice:");
		choice=sc.nextInt();
		while(choice!=5){
			switch(choice){
			case 1:
				createMap();
				break;
			case 2:
				iterateMap();
				break;
			case 3:
				sortMap();
				break;
			case 4:
				sortByValues();
				break;
			case 5:
				break;
			}			
			System.out.println("Menu:");
			System.out.println("1. Create Map");
			System.out.println("2. Iterate Map");
			System.out.println("3. Sort by Keys");
			System.out.println("4. Sort by Values");
			System.out.println("5. Exit");
			System.out.print("Choice:");
			choice=sc.nextInt();
		}
	}
	private static void createMap(){
		map.put(3,"COD");  
	    map.put(1,"Assassin Creed");  
	    map.put(5,"Crysis3");  
	    map.put(2,"Dragon Ball Xenoverse");  
	    System.out.println("Created Successfully!");
	    System.out.println(map);
	}
	private static void iterateMap(){
	    Set set=map.entrySet();
	    Iterator itr=set.iterator();  
	    while(itr.hasNext()){  
	        Map.Entry entry=(Map.Entry)itr.next();  
	        System.out.println(entry.getKey()+" "+entry.getValue());  
	    }
	}
	private static void sortMap() {
		System.out.println("Orignal HashMap:" + map);
		TreeMap<Integer, String>mapSorted = new TreeMap<>(map);
		map.forEach((key, value) -> {
			System.out.println(key + ": " + value);
		});
	} 
	private static void sortByValues() { 
	       List list = new LinkedList(map.entrySet());
	       Collections.sort(list, new Comparator() {
	            public int compare(Object o1, Object o2) {
	               return ((Comparable) ((Map.Entry) (o1)).getValue())
	                  .compareTo(((Map.Entry) (o2)).getValue());
	            }
	       });
	       HashMap sortedHashMap = new LinkedHashMap();
	       for (Iterator it = list.iterator(); it.hasNext();) {
	              Map.Entry entry = (Map.Entry) it.next();
	              sortedHashMap.put(entry.getKey(), entry.getValue());
	       } 
	       System.out.println("After Sorting:");
	       Set set2 = sortedHashMap.entrySet();
	       Iterator iterator2 = set2.iterator();
	       while(iterator2.hasNext()) {
	            Map.Entry me2 = (Map.Entry)iterator2.next();
	            System.out.print(me2.getKey() + ": ");
	            System.out.println(me2.getValue());
	       }
	  }
}
