package bouncyCastle;
import java.security.Key;
import java.security.Security;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.crypto.examples.DESExample;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.pqc.jcajce.provider.BouncyCastlePQCProvider;
import sun.misc.*;
import java.lang.*;
import java.util.*;
import java.io.BufferedReader;
import java.io.FileReader;

public class App
{
	private static String algo[]={"AES","AES","SEED","Rijndael","Skipjack","Twofish","XTEA","Serpent"};
	private static String algorithm;
	private static byte[] keyValue=new byte[] {'0','2','3','4','5','6','7','8','9','1','2','3','4','5','6','7'};
    public static String encrypt(String plainText,int n) throws Exception {
        	algorithm=algo[n];
            Key key = generateKey();
            System.out.println("Secret Key Length:"+key.getEncoded().length);
            Security.addProvider(new BouncyCastleProvider());
            Cipher chiper = Cipher.getInstance(algorithm);
            chiper.init(Cipher.ENCRYPT_MODE, key);
            byte[] encVal = chiper.doFinal(plainText.getBytes());
            String encryptedValue = new BASE64Encoder().encode(encVal);
            return encryptedValue;
    }
    private static Key generateKey() throws Exception 
    {
            Key key = new SecretKeySpec(keyValue, algorithm);
            return key;
    }
    public static void main(String[] args) throws Exception {
    		Random rand = new Random();
    		int  n = rand.nextInt(8) + 1;
    		Scanner sc=new Scanner(System.in);
    		System.out.println("String:");
    		String text=sc.nextLine();
            String plainText = text;
            String encryptedText = encrypt(plainText,n);
            System.out.println("Algorithm: "+algo[n]);
            System.out.println("Plain Text : " + plainText);
            System.out.println("Encrypted Text : " + encryptedText);
            System.out.println("Plain Text Length: " + plainText.getBytes().length);
            System.out.println("Encrypted Text Length: " + encryptedText.getBytes().length);
    }
}